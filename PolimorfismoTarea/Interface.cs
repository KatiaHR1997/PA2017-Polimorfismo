﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolimorfismoTarea
{
    interface Interface<T>
    {
        void agregar(T item);
        void listar(string item);
    }
}
