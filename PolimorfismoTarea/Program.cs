﻿using System;

namespace PolimorfismoTarea
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaDatos<Estudiante> estudiantes = new ListaDatos<Estudiante>();
            ListaDatos<Curso> cursos = new ListaDatos<Curso>();

            Console.WriteLine(">>> Datos Estudiantes <<<\n");
            for (int i = 1; i <= 5; i++)
            {
                Estudiante estudiante = new Estudiante();
                estudiante.NombreEstudiante = "Estudiante " + i;
                estudiantes.agregar(estudiante);
                estudiantes.listar(estudiante.NombreEstudiante);
            }

            Console.WriteLine("\n\n>>> Datos Cursos <<<\n");
            for (int i = 1; i <= 5; i++)
            {
                Curso curso = new Curso();
                curso.NombreCurso = "Curso " + i;
                cursos.agregar(curso);
                cursos.listar(curso.NombreCurso);
            }

            Console.ReadKey();
        }
    }
}