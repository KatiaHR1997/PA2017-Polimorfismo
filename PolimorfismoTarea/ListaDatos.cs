﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolimorfismoTarea
{
    class ListaDatos<T> : Interface<T>
    {
        public List<T> lista { get; set; }
        public ListaDatos()
        {
            this.lista = new List<T>();
        }

        public void agregar(T item)
        {
            lista.Add(item);
        }
        public void listar(string item)
        {
            Console.WriteLine(item);
        }
    }
}
